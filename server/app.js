require('dotenv').load();

const path = require("path");
const express = require("express");
const app = express();
const rp = require('request-promise');

const NODE_PORT = process.env.port || 3000;

const handleError = function (err, res) {
    res.status(500).type("text/plain").send(JSON.stringify(err));
}

const CLIENT_FOLDER = path.join(__dirname, "/../client/");
app.use(express.static(CLIENT_FOLDER));

const BOWER_FOLDER = path.join(__dirname, "/../client/bower_components/");
app.use("/libs", express.static(BOWER_FOLDER));

const IMG_FOLDER = path.join(__dirname, "/img/");
app.use("/img", express.static(IMG_FOLDER));


app.get("/lists", function (req, res) {
    
        var options = {
            uri: "https://api.nytimes.com/svc/books/v3/lists/names.json",
            qs: {
                'api-key': process.env.NYT_BOOKS_API_KEY
            },
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        };
    
        rp(options)
            .then(function (bestsellers) {
                // console.log("#results: " + bestsellers.num_results);
                res.status(200).send(bestsellers);
            })
            .catch(function (err) {
                // API call failed...
            });
    });

app.get("/bestsellers", function (req, res) {

    var options = {
        uri: "https://api.nytimes.com/svc/books/v3/lists/best-sellers/history.json",
        qs: {
            'api-key': process.env.NYT_BOOKS_API_KEY
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(function (bestsellers) {
            // console.log("#results: " + bestsellers.num_results);
            res.status(200).send(bestsellers);
        })
        .catch(function (err) {
            // API call failed...
        });
});

//catch all
app.use(function (req, res) {
    console.info("404 Method %s, Resource %s", req.method, req.originalUrl);
    res.status(404).type("text/html").send("<h1>404 Resource not found</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app