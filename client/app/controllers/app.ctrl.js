(function () {
    'use strict';

    angular
        .module('MyApp')
        .controller('MyCtrl', MyCtrl);

    MyCtrl.$inject = ["$scope", '$http', "$ionicListDelegate", "$ionicLoading"];
    function MyCtrl($scope, $http, $ionicListDelegate, $ionicLoading) {
        var vm = this;
        vm.items = [];
        vm.shouldShowDelete = false;
        vm.shouldShowReorder = false;
        vm.listCanSwipe = true;
        vm.toggleDelete = toggleDelete;
        vm.deleteItem = deleteItem;
        vm.share = share;
        vm.reorderItem = reorderItem;
        vm.reload = reload;
    

        init();

        ////////////////

        function reload() {
            vm.items = [];
            $http.get("/bestsellers").then(function (result) {
                console.log(result.data);
                vm.items = result.data.results;
                $scope.$broadcast("scroll.refreshComplete");                
            }).catch(function (err) {
                console.log(err);
            });
        }

        function reorderItem(item, from, to) {
            vm.items[from] = vm.items[to];
            vm.items[to] = item;
        }

        function init() {
            $ionicLoading.show({
                template : "Loading"
            });

            $http.get("/bestsellers").then(function (result) {
                console.log(result.data);
                vm.items = result.data.results;
                $ionicLoading.hide();
                // const book = vm.list[0];
                // console.log(book);
            }).catch(function (err) {
                console.log(err);
            });

        }

        function share(i) {
            console.log(i);
            $ionicListDelegate.closeOptionButtons();
        }


        function deleteItem(i) {
            vm.items.splice(i, 1);
        }

        function toggleDelete() {
            vm.ctrl.shouldShowDelete = !vm.ctrl.shouldShowDelete;
        }
    }
})();